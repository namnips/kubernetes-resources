# Create Kubernetes cluster on Ubuntu ARM64 VMs

Set up 3 Ubuntu Server ARM64 VMs on Parallels Desktop - MacBook Pro M series chip

- 1 master, has 2 CPUs and 2 GBs RAM
- 2 workers, each worker has 2 CPUs and 2 GBs RAM

## Provision VMs with vagrant

```shell
cd 1-provision-ubuntu-vm
vagrant up
```

## Remove VMs with vagrant

```shell
cd 1-provision-ubuntu-vm
vagrant destroy -f
```
