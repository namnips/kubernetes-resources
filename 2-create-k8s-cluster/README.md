# Create Kubernetes cluster on Ubuntu ARM64 VMs

Set up a new Kubernetes cluster that has

- 1 master, has 2 CPUs and 2 GBs RAM
- 2 workers, each worker has 2 CPUs and 2 GBs RAM

## Create Kubernetes cluster

### step by step follow my video

Clone course Git repository:

```shell
git clone https://github.com/namnips/cka
```

• Install CRI:

```shell
sudo ./setup-container.sh
```

• Install kubetools:

```shell
sudo ./setup-kubetools.sh
```

• Install the cluster:

```shell
sudo kubeadm init
```

• Set up the client:
• mkdir ~/.kube
• sudo cp-i /etc/kubernetes/admin.conf~/.kube/config
• sudo chown $(id -u):$(id -g) .kube/config
• Join other nodes: (on other nodes) sudo kubeadm join < join-token>

### install calico

```shell
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.0/manifests/calico.yaml
```

For more information, see the instructions at https://docs.tigera.io/calico/latest/getting-started/kubernetes/quickstart to install the Calico network plugin.

```
Let's ssh into the master VM, and verify the cluster
```

```shell
ssh ci@172.16.1.11
kubectl get nodes
```

## Download kubeconfig to your local

```shell
mkdir ~/.kube-local
scp ci@172.16.1.11:/home/ci/.kube/config ~/.kube-local/config
export KUBECONFIG=~/.kube-local/config && kubectl get nodes
```

Thank you Sander Van Vugt
